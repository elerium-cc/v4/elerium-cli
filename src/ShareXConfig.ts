import Config from "./interfaces/ShareXConfig";

export default class ShareXConfigGen {
    data: Config

    constructor(url: string, method: string) {
        this.data = {
            DestinationType: "ImageUploader",
            RequestType: method,
            RequestURL: url,
            Name: "Generated-Config",
            FileFormName: "file",
            Body: "MultipartFormData"
        }
    }

    setName(name: string) {
        this.data.Name = name;
    }

    /**
     * @description Ugly code and repeated on other properties
     */
    setArgument(name: string, value: any) {
        const { data } = this;
        if (!data.Arguments) data.Arguments = {};

        data.Arguments[name] = value;
    }

    setParameter(name: string, value: any) {
        const { data } = this;
        if (!data.Parameters) data.Parameters = {};

        data.Parameters[name] = value;
    }

    setHeader(name: string, value: any) {
        const { data } = this;
        if (!data.Arguments) data.Headers = {};

        data.Headers[name] = value;
    }

    setBodyType(type: string) {
        this.data.Body = type;
    }

    setDestinationType(types: String[]) {
        this.data.DestinationType = types.join(", ");
    }

    setFileFormName(name: string) {
        this.data.FileFormName = name;
    }


    setUrl(url: string) {
        this.data.URL = url;
    }

    generateJson() : string {
        return JSON.stringify(this.data, null, "\t");
    }
}
