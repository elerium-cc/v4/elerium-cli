export default interface ShareXConfig {
    Headers?: any
    DestinationType: string
    RequestType: string
    RequestURL: string
    Name: string
    Parameters?: any
    Body: string
    FileFormName: string
    URL?: string
    ThumbnailURL?: string
    DeletionURL?: string
    ErrorMessage?: string
    Arguments?: any
};