import yargs from "yargs";
import APIWrapper from "./api/index";
import { 
    writeFile
} from "fs/promises";
import {
    readFileSync
} from "fs";
let token;

const config = require("/snapshot/elerium-cli/config.json");


try { 
    token = readFileSync("./token.txt", "ascii");
} catch(err) {};


const APIClient = new APIWrapper(config.url, token);
yargs(process.argv.slice(2))
    .command("login", "logins to your account", (yargs: any) => {
        yargs.option("username", {
            alias: "u",
            demandOption: true
        });
        yargs.option("password", {
            alias: "p",
            demandOption: true
        });
    }, async (args: any) => {
        const [ success, response ] = await APIClient.login(args.username, args.password);

        if (success) {
            await writeFile("./token.txt", response);
        } else {
            console.log(response);
        };
    })
    .command("get-current-user", "gets your user", () => {}, async (args) => {
        try {
            const user = await APIClient.getCurrentUser();

            for (const prop in user) {
                if (typeof user[prop] !== "object") {
                    console.log(prop + ":", user[prop]);
                }; 
            };
        } catch(err) {
            console.log(err.message);
        }
    })
    .command("generate-config", "generates a config", () => {}, async () => {
        const config = await APIClient.generateUploadConfig();

        process.stdout.write(config);
    })
    .command("generate-upload-key", "generates a upload key", () => {}, async () => { 
        try {
            const key = await APIClient.generateUploadKey();

            console.log(key);
        } catch(err) {
            console.log(err.message);
        }
    })
    .command("set-setting", "sets a setting on your acc", (yargs: any) => {
        yargs.option("type", {
            alias: "t",
            demandOption: true
        });
        yargs.option("value", {
            alias: "v",
            demandOption: true
        });
    }, async (args: any) => {
        try {
            const response = await APIClient.setSetting(args.type, args.value);
            console.log(response.message);
        } catch(err) {
            console.log(err.message);
        }
    })
    .command("set-password", "changes your pw", (yargs: any) => {
        yargs.option("password", {
            alias: "p",
            demandOption: true
        });
    }, async (args: any) => {
        try {
            const response = await APIClient.setPassword(args.password);

            console.log(response.message);
        } catch(err) {
            console.log(err.message);
        }
    })
    .help()
    .argv