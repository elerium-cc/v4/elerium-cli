import fetch, { 
    RequestInfo, 
    RequestInit,
    Response
} from "node-fetch";
import ShareXConfig from "../ShareXConfig";


export default class APIWrapper {
    token?: string;
    apiUrl: string;

    constructor(apiUrl: string, token?: string) {
        if (token) this.token = token;
        this.apiUrl = apiUrl;
    };

    private async request(input: RequestInfo, init: RequestInit): Promise<Response> {
        if (!init.headers) {
            init.headers = {}
        };

        init!.headers = < { [key: string]: string }>init.headers;

        if (this.token && init && init.headers && !init.headers["Authorization"]) {
            init.headers["Authorization"] = this.token;
        };

        return await fetch(input, init);
    };

    private checkToken() {
        if (!this.token) throw new Error("Token is required");
    }

    async login(name: string, password: string): Promise<[ boolean, string ]> {
        const response = await this.request(`${this.apiUrl}/users/login`, {
            method: "POST",
            body: JSON.stringify({
                name,
                password
            }),
            headers: { 
                "Content-Type": "application/json"
            }
        });

        const { 
            token,
            success,
            message
        } = await response.json();

        return [
            success,
            success && token || message
        ];
    };

    async getCurrentUser() {
        this.checkToken();

        const user = await this.request(`${this.apiUrl}/users/@me`, {});
        const userRes = await user.json();

        if (!userRes.success) throw new Error(userRes.message);

        return userRes.user;
    };


    async generateUploadConfig() {
        const user = await this.getCurrentUser();

        if (!user.uploadKey) throw new Error("The upload key doesn't exist");

        const config = new ShareXConfig(`${this.apiUrl}/files/upload`, "POST");
        config.setHeader("Authorization", user.uploadKey);
        config.setName("elerium.cc");
        config.setUrl("$json:url$");

        return config.generateJson();
    };

    async generateUploadKey() {
        this.checkToken();
        const response = await this.request(`${this.apiUrl}/users/@me/auth/uploadkey/generate`, {
            method: "POST"
        });
        const jsonResponse = await response.json();

        if (jsonResponse.success) return jsonResponse.key;


    };

    async setSetting(type: string, value: any) {
        this.checkToken();
        const response = await this.request(`${this.apiUrl}/users/@me/settings/set`, {
            method: "POST",
            body: JSON.stringify({
                type,
                value
            }),
            headers: {
                "Content-Type": "application/json"
            }
        });
      
        const jsonResponse = await response.json();
        return jsonResponse;

    };

    async setPassword(password: string) {
        this.checkToken();
        const response = await this.request(`${this.apiUrl}/users/@me/password/set`, {
            method: "POST",
            body: JSON.stringify({
                password
            }),
            headers: {
                "Content-Type": "application/json"
            }
        });

        const jsonResponse = await response.json();
        return jsonResponse;

    };

}